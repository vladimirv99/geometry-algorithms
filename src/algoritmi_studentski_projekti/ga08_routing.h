#ifndef GA08_ROUTING_H
#define GA08_ROUTING_H

#include "algoritambaza.h"
#include <QPointF>
#include <QLineF>
#include <vector>
#include <unordered_map>

using Cvor = QPointF;
using Grana = QLineF;

constexpr unsigned M = 6;
constexpr double THETA_ANGLE = 360.0/M;

double ugao(const Cvor *A, const Cvor *B, const Cvor *C);
bool eps_eq(double lhs, double rhs);
std::pair<unsigned, double> region(const Cvor *trenutni, const Cvor *ciljni);
double udaljenost(const Cvor* trenutni, const Cvor* cvor);

struct Prepreka
{
public:
    Prepreka(const QPointF* p1, const QPointF* p2);
    bool sece(const QPointF* start, const QPointF* end) const;
    const Cvor *drugaStrana(const Cvor *cvor) const;
    void crtaj(QPainter *painter) const { painter->drawLine(_line); };

    inline const Cvor *p1() const { return _p1; };
    inline const Cvor *p2() const { return _p2; };
private:
    const QPointF* _p1;
    const QPointF* _p2;
    QLineF _line;
};

struct Graf
{
    Graf(const std::string &imeDatoteke = "", int brojCvorova = BROJ_SLUCAJNIH_OBJEKATA, unsigned width = CANVAS_WIDTH, unsigned height = CANVAS_HEIGHT);

    inline const Cvor *cvor(unsigned idx) const { return &_cvorovi[idx]; };
    inline const std::vector<Cvor> &cvorovi() const { return _cvorovi; };
    inline const Prepreka *prepreke(unsigned idx) const { return &_prepreke[idx]; };
    inline const std::vector<Prepreka> &prepreke() const { return _prepreke; };

    inline const Cvor *pocetak() const { return _pocetak; };
    inline const Cvor *kraj() const { return _kraj; };
    bool secePrepreku(const QPointF* pocetak, const QPointF* kraj) const;

private:
    void generisiNasumican(unsigned brojCvorova, unsigned brojPrepreka, unsigned width, unsigned height);
    void ucitajIzFajla(const std::string &imeDatoteke);

    std::vector<Cvor> _cvorovi;
    std::vector<Prepreka> _prepreke;
    const Cvor *_pocetak = nullptr;
    const Cvor *_kraj = nullptr;
};

struct Podregion
{
    Podregion(double ugaoPocetka, double ugaoKraja);
    bool pripada(double ugao) const;
    void azurirajNajblizi(double ugao, double udaljenost, const Cvor *cvor);
    void dodajNajblizi();
    bool dodajCvor(double ugao, const Cvor *cvor);

    inline const Cvor *najblizi() const { return _najbliziCvor; };
    inline const std::vector<std::pair<double, const Cvor *>> &susedi() const {return _susedi;};
private:
    double _ugaoPocetka;
    double _ugaoKraja;

    double _najmanjaUdaljenost;
    double _ugaoNajblizeg;
    const Cvor *_najbliziCvor;

    std::vector<std::pair<double, const Cvor *>> _susedi;
};

struct Region
{
    Region(const Cvor *centar, unsigned ugaoSredine, std::vector<double> &ugloviPodregiona);
    void azurirajNajblizi(const Cvor *noviCvor, double ugao);
    void dodajNajblize();
    void dodajCvor(const Cvor *noviCvor, double ugao);

    const Cvor *najbliziNaSmeru(double ugao) const;
    inline const std::vector<Podregion> &podregioni() const {return _podregioni;};

    void crtajNajblize(QPainter *painter) const;
    void crtajSusede(QPainter *painter) const;
private:
    const Cvor *_centar;
    double _ugaoSredine;
    std::vector<Podregion> _podregioni;
};

enum class TipOkoline
{
    CONSTRAINED_THETA_6,
    VISIBILITY_GRAPH
};

struct Okolina
{
    Okolina(const Graf &graf, const Cvor *cvor, TipOkoline tip);
    const Cvor *sledeciValidan(const QPointF *destinacija) const;
    std::pair<const Cvor *, const Cvor *> kandidatiZaIzbegavanjePrepreke(unsigned regionPrepreke) const;

    std::vector<const Cvor *> najblizi() const;
    inline const std::vector<const Prepreka *> &prepreke() const {return _prepreke;};

    void crtajNajblize(QPainter *painter) const;
    void crtajSusede(QPainter *painter) const;
private:
    const Graf &_graf;
    const Cvor *_centar;
    std::vector<Region> _regioni;
    std::vector<const Prepreka *> _prepreke;
};

class Routing  : public AlgoritamBaza
{
public:
    Routing(QWidget *pCrtanje,
            int pauzaKoraka,
            const bool &naivni = false,
            std::string imeDatoteke = "",
            int brojTacaka = BROJ_SLUCAJNIH_OBJEKATA);

    void pokreniAlgoritam() override;
    void crtajAlgoritam(QPainter *painter) const override;
    void pokreniNaivniAlgoritam() override;
    void crtajNaivniAlgoritam(QPainter *painter) const override;

    std::vector<unsigned> pronadjeniPut() const;
    void stampajGraf() const;

private:
    unsigned int _n;

    // Ulaz
    Graf _graf;

    // Podaci za efikasni algoritam
    std::unordered_map<const Cvor *, Okolina> _okoline; // Okoline cvorova u grafu
    std::vector<const Cvor*> _put; // Trenutni predjeni put, samo za iscrtavanje
    const Cvor *_pocetakFazeIzbegavanja = nullptr; // Cvor koji je izazvao izbegavanje prepreke

    // Podaci za naivni algoritam
    std::vector<const Cvor *> _stek;
    std::unordered_map<const Cvor *, std::vector<const Cvor *>> _grane;
    std::unordered_map<const Cvor *, const Cvor *> _roditelj;
    std::unordered_map<const Cvor *, bool> _posecen;
};

#endif // GA08_ROUTING_H
