#include "ga08_routing.h"

#include <iostream>
#include <fstream>
#include <limits>
#include <QDebug>
#include "pomocnefunkcije.h"

// Racunanje ugla ABC
double ugao(const Cvor *A, const Cvor *B, const Cvor *C)
{
    QLineF theta_start(*B, *A);
    QLineF current(*B, *C);
    return 360.0f - theta_start.angleTo(current);
}

// Poredjenje jednakosti uz preciznost
bool eps_eq(double lhs, double rhs)
{
    return fabs(lhs - rhs) < EPS;
}

// Racunanje regiona i ugla od leve granice prvog regiona za ciljni cvor u odnosu na trenutni
std::pair<unsigned, double> region(const Cvor *trenutni, const Cvor *ciljni)
{
    double ugaoPrvogRegiona = (90.0 + (THETA_ANGLE / 2)) / 180.0 * M_PI;
    QPointF pocetakPrvogRegiona(trenutni->x()+10*cos(ugaoPrvogRegiona), trenutni->y()+10*sin(ugaoPrvogRegiona));
    double ugaoIzmedjuTacaka = 360.0 - ugao(&pocetakPrvogRegiona, trenutni, ciljni);
    return {static_cast<unsigned>(ugaoIzmedjuTacaka / THETA_ANGLE), ugaoIzmedjuTacaka};
}

// Racunanje udaljenosti ciljnog cvora od trenutnog duz projekcije na polupravu koja polovi region
double udaljenost(const Cvor* trenutni, const Cvor* ciljni)
{
    auto regionCilja = region(trenutni, ciljni);
    double razlikaUglova = (regionCilja.second - (regionCilja.first * THETA_ANGLE + THETA_ANGLE / 2)) / 180.0 * M_PI;
    QLineF linija = QLineF(*trenutni, *ciljni);
    double udaljenost = linija.length() * cos(razlikaUglova);
    return udaljenost;
}

Prepreka::Prepreka(const QPointF* p1, const QPointF* p2)
    : _p1(p1), _p2(p2), _line(*p1, *p2)
{

}

// Provera da li duz pocetak-kraj sece prepreku
bool Prepreka::sece(const QPointF* pocetak, const QPointF* kraj) const
{
    if(p1() == pocetak || p1() == kraj || p2() == pocetak || p2() == kraj)
        return false;

    QLineF trenutna_duz = QLineF(*pocetak, *kraj);
    QPointF presek;
    auto tip_preseka = _line.intersects(trenutna_duz, &presek);
    return tip_preseka == QLineF::IntersectType::BoundedIntersection;
}

// Dohvatanje suprotnog kraja prepreke od prosledjenog
const Cvor *Prepreka::drugaStrana(const Cvor *cvor) const
{
    if (cvor == _p1)
    {
        return _p2;
    }
    else if (cvor == _p2)
    {
        return _p1;
    }
    else
    {
        // Prepreka nije incidentna cvoru
        return nullptr;
    }
}

Podregion::Podregion(double ugaoPocetka, double ugaoKraja)
    : _ugaoPocetka(ugaoPocetka), _ugaoKraja(ugaoKraja), _najmanjaUdaljenost(std::numeric_limits<double>::max()), _najbliziCvor(nullptr)
{

}

void Podregion::azurirajNajblizi(double ugao, double udaljenost, const Cvor *cvor)
{
    // Ako pripada ovom podregionu i ima manju udaljenost od trenutnog cvora
    // onda zameni trenutni cvor novim
    if (pripada(ugao) && udaljenost < _najmanjaUdaljenost)
    {
        _najbliziCvor = cvor;
        _najmanjaUdaljenost = udaljenost;
        _ugaoNajblizeg = ugao;
    }
}

// Dodavanje cvora u listu suseda
bool Podregion::dodajCvor(double ugao, const Cvor *cvor)
{
    // Ako pripada ovom podregionu dodaj ga u listu suseda
    if (pripada(ugao))
    {
        _susedi.emplace_back(ugao, cvor);
        return true;
    }
    return false;
}

// Dodavanje najblizeg cvora u listu suseda
void Podregion::dodajNajblizi()
{
    if (_najbliziCvor)
    {
        // Provera da li je on vec u listi jer mu je ovaj cvor najblizi
        bool sadrziNajblizeg = false;
        for (const auto& sused : _susedi)
        {
            if (sused.second == _najbliziCvor)
            {
                sadrziNajblizeg = true;
                break;
            }
        }
        if (!sadrziNajblizeg)
            _susedi.emplace_back(_ugaoNajblizeg, _najbliziCvor);
    }
}

bool Podregion::pripada(double ugao) const
{
    return _ugaoPocetka - EPS < ugao && ugao < _ugaoKraja + EPS;
}

Region::Region(const Cvor *centar, unsigned ugaoSredine, std::vector<double> &ugloviPodregiona)
    : _centar(centar), _ugaoSredine(ugaoSredine)
{
    // Uglovi se sortiraju radi lakse podele na podregione
    std::sort(ugloviPodregiona.begin(), ugloviPodregiona.end());

    // Konstrukcija podregiona koji su podeljeni po uglovima iz ugloviPodregiona
    double minUgao = ugaoSredine - THETA_ANGLE/2;
    for (const auto ugao : ugloviPodregiona)
    {
        _podregioni.emplace_back(minUgao, ugao);
        minUgao = ugao;
    }
    _podregioni.emplace_back(minUgao, ugaoSredine + THETA_ANGLE/2);
}

void Region::azurirajNajblizi(const Cvor *noviCvor, double ugao)
{
    double razlikaUglova = (ugao - _ugaoSredine) / 180.0 * M_PI;
    QLineF linija = QLineF(*_centar, *noviCvor);
    double udaljenost = linija.length() * cos(razlikaUglova);

    // Jedan ugao moze da bude u 1 ili 2 podregiona
    for (auto& podregion : _podregioni)
    {
        podregion.azurirajNajblizi(ugao, udaljenost, noviCvor);
    }
}

void Region::dodajCvor(const Cvor *noviCvor, double ugao)
{
    // Jedan ugao moze da bude u 1 ili 2 podregiona
    for (auto& podregion : _podregioni)
    {
        podregion.dodajCvor(ugao, noviCvor);
    }
}

void Region::dodajNajblize()
{
    for (auto &podregion : _podregioni)
    {
        podregion.dodajNajblizi();
    }
}

const Cvor *Region::najbliziNaSmeru(double ugao) const
{
    for (const auto &podregion : _podregioni)
    {
        if (podregion.pripada(ugao) && podregion.najblizi())
        {
            return podregion.najblizi();
        }
    }
    return nullptr;
}

void Region::crtajNajblize(QPainter *painter) const
{
    for (const auto &podregion : _podregioni)
    {
        // Mozda ne postoji najblizi
        if (podregion.najblizi())
            painter->drawLine(*_centar, *podregion.najblizi());
    }
}

void Region::crtajSusede(QPainter *painter) const
{
    for (const auto &podregion : _podregioni)
    {
        for (const auto &sused : podregion.susedi())
        {
            painter->drawLine(*_centar, *sused.second);
        }
    }
}

Okolina::Okolina(const Graf &graf, const Cvor *centar, TipOkoline tip)
    : _graf(graf), _centar(centar)
{
    // Odredjivanje svih prepreka incidentnih sa trenutnim cvorom i njihovog suprotnog kraja
    // uglovi prepreka su podeljeni po regionima u kojima se nalaze
    std::vector<std::vector<double>> ugloviIncidentnihPrepreka (M);
    for (const auto& prepreka : _graf.prepreke())
    {
        const Cvor *drugaStrana = prepreka.drugaStrana(_centar);
        if (!drugaStrana)
        {
            // Prepreka nije incidentna cvoru, preskacemo
            continue;
        }

        // Dodavanje u listu incidentnih prepreka
        _prepreke.push_back(&prepreka);

        // Racunanje ugla koji prepreka zaklapa sa cvorom
        auto regionPrepreke = region(_centar, drugaStrana);
        ugloviIncidentnihPrepreka[regionPrepreke.first].push_back(regionPrepreke.second);
    }

    // Inicijalizacija regiona
    _regioni.reserve(M);
    for (unsigned idRegiona = 0; idRegiona < M; idRegiona++)
    {
        _regioni.emplace_back(_centar, THETA_ANGLE/2 + idRegiona * THETA_ANGLE, ugloviIncidentnihPrepreka[idRegiona]);
    }
    ugloviIncidentnihPrepreka.clear();

    // Dodavanje cvorova i formiranje grana
    switch(tip)
    {
    case TipOkoline::CONSTRAINED_THETA_6:
        // Ograniceni teta-6 graf
        for (const auto &susedniCvor : _graf.cvorovi())
        {
            // preskacemo trenutni cvor
            if (_centar == &susedniCvor)
                continue;

            if (!_graf.secePrepreku(_centar, &susedniCvor))
            {
                auto regionCvora = region(_centar, &susedniCvor);
                _regioni[regionCvora.first].azurirajNajblizi(&susedniCvor, regionCvora.second);

                // Provera grane sa druge strane
                // Potrebno je da ne postoji nijedan cvor u ogranicenom kanonskom trouglu od suseda koji proveravamo do ovog cvora
                // takav da ga trenutni cvor moze videti
                bool jeSused = true;
                auto regionSaCvora = region(&susedniCvor, _centar);
                for (const auto &kandidatZaSuseda : _graf.cvorovi())
                {
                    if (&kandidatZaSuseda == &susedniCvor || &kandidatZaSuseda == _centar)
                        continue;

                    auto regionKandidata = region(&susedniCvor, &kandidatZaSuseda);
                    if (
                        regionKandidata.first != regionSaCvora.first ||
                        udaljenost(&susedniCvor, &kandidatZaSuseda) > udaljenost(&susedniCvor, _centar) ||
                        _graf.secePrepreku(_centar, &kandidatZaSuseda)
                    )
                        continue;
                    bool jeValidna = true;
                    for (const auto prepreka : _prepreke)
                    {
                        if(pomocneFunkcije::orijentacija(*_centar, *prepreka->drugaStrana(_centar), susedniCvor) != pomocneFunkcije::orijentacija(*_centar, *prepreka->drugaStrana(_centar), kandidatZaSuseda))
                        {
                            jeValidna = false;
                        }
                    }
                    if(!jeValidna)
                        continue;
                    jeSused = false;
                    break;
                }
                if (jeSused)
                {
                    _regioni[regionCvora.first].dodajCvor(&susedniCvor, regionCvora.second);
                }
            }
        }

        for (auto &region : _regioni)
        {
            region.dodajNajblize();
        }
        break;
    case TipOkoline::VISIBILITY_GRAPH:
        // Graf vidljivosti
        for (const auto &susedniCvor : _graf.cvorovi())
        {
            // preskacemo trenutni cvor
            if (_centar == &susedniCvor)
                continue;

            if (!_graf.secePrepreku(_centar, &susedniCvor))
            {
                auto regionCvora = region(_centar, &susedniCvor);
                _regioni[regionCvora.first].azurirajNajblizi(&susedniCvor, regionCvora.second);
                _regioni[regionCvora.first].dodajCvor(&susedniCvor, regionCvora.second);
            }
        }
    }
}

const Cvor *Okolina::sledeciValidan(const Cvor *destinacija) const
{
    // Ako postoji grana u podregionu sa destinacijom koja se cela nalazi u kanonskom trouglu, idemo tom granom
    // u suprotnom prelazimo u fazu izbegavanja prepreke
    auto regionDestinacije = region(_centar, destinacija);
    const Cvor *sledeci = _regioni[regionDestinacije.first].najbliziNaSmeru(regionDestinacije.second);

    if (sledeci)
    {
        // Provera pripadnosti kanonskom trouglu cvor,destinacija,regionDestinacije.first
        if (udaljenost(_centar, sledeci) <= udaljenost(_centar, destinacija))
            return sledeci;
        else
            return nullptr;
    }
    else
    {

        return nullptr;
    }
}

std::pair<const Cvor *, const Cvor *> Okolina::kandidatiZaIzbegavanjePrepreke(unsigned regionPrepreke) const
{
    // Kandidat 1 (najblizi)
    auto regionKandidata1 = (regionPrepreke + 2) % M;
    const Cvor *kandidat1 = _regioni[regionKandidata1].podregioni()[0].najblizi();

    // Kandidat 2 (najmanji ugao)
    auto regionKandidata2 = (regionPrepreke + 1) % M;
    const Cvor *kandidat2 = nullptr;
    double najmanjiUgao = std::numeric_limits<double>::max();
    const auto& grane = _regioni[regionKandidata2].podregioni()[0].susedi();
    for (const auto& grana : grane)
    {
        if (grana.first < najmanjiUgao)
        {
            kandidat2 = grana.second;
            najmanjiUgao = grana.first;
        }
    }

    return {kandidat1, kandidat2};
}

std::vector<const Cvor *> Okolina::najblizi() const
{
    std::vector<const Cvor *> susedi;
    for (const auto &region : _regioni)
    {
        const auto &podregioni = region.podregioni();
        for(const auto &podregion : podregioni)
        {
            if (podregion.najblizi())
                susedi.push_back(podregion.najblizi());
        }
    }

    return susedi;
}

void Okolina::crtajNajblize(QPainter *painter) const
{
    for (const auto &region : _regioni)
    {
        region.crtajNajblize(painter);
    }
}

void Okolina::crtajSusede(QPainter *painter) const
{
    for (const auto &region : _regioni)
    {
        region.crtajSusede(painter);
    }
}

Graf::Graf(const std::string &imeDatoteke, int brojCvorova, unsigned width, unsigned height)
{
    if (imeDatoteke == "") {
        generisiNasumican(brojCvorova, brojCvorova / 4, width, height);
    } else {
        ucitajIzFajla(imeDatoteke);
    }
}

void Graf::generisiNasumican(unsigned brojCvorova, unsigned brojPrepreka, unsigned width, unsigned height)
{
    srand(static_cast<unsigned>(time(nullptr)));

    if (brojCvorova < 2)
      brojCvorova = 2;

    if (brojPrepreka > brojCvorova / 2)
        brojPrepreka = brojCvorova / 2;

    // Kreiranje cvorova
    for(unsigned i = 0; i < brojCvorova; i++)
    {
        // Koordinate se mnoze i dele sa 10 da bi se dodala jedna decimala
        _cvorovi.emplace_back(rand()%(width*10)/10.0, rand()%(height*10)/10.0);
    }

    // Izbor pocetnog i krajnjeg cvora
    _pocetak = &_cvorovi[0];
    _kraj = &_cvorovi[1];

    // Kreiranje prepreka
    unsigned n_prepreke = 0;
    while (n_prepreke < brojPrepreka)
    {
        unsigned idx1 = rand()%_cvorovi.size();
        unsigned idx2 = rand()%_cvorovi.size();

        // Provera da li je prepreka validna
        if(
            idx1 == idx2 ||
            secePrepreku(&_cvorovi[idx1], &_cvorovi[idx2])
        ) {
            continue;
        } else {
            _prepreke.emplace_back(&_cvorovi[idx1], &_cvorovi[idx2]);
            n_prepreke++;
        }
    }
}

void Graf::ucitajIzFajla(const std::string &imeDatoteke)
{
    std::ifstream in(imeDatoteke);

    // Ucitavanje broja cvorova i prepreka
    unsigned brojCvorova, brojPrepreka;
    in >> brojCvorova >> brojPrepreka;

    if (brojCvorova < 2)
    {
        qFatal("Greska! Nedovoljan broj cvorova");
    }

    // Ucitavanje cvorova
    _cvorovi.reserve(brojCvorova);
    double x, y;
    for (unsigned i = 0; i < brojCvorova; i++)
    {
        in >> x >> y;
        _cvorovi.emplace_back(x, y);
    }

    // Ucitavanje prepreka
    unsigned p1, p2;
    for (unsigned i = 0; i < brojPrepreka; i++)
    {
        // Ucitavanje indeksa cvorova koji su krajevi prepreke
        in >> p1 >> p2;
        // Provera da li je prepreka validna
        if (p1 != p2 && p1 > 0 && p1 < _cvorovi.size() && p2 > 0 && p2 < _cvorovi.size() &&
            !secePrepreku(&_cvorovi[p1], &_cvorovi[p2])
        )
        {
            _prepreke.emplace_back(&_cvorovi[p1], &_cvorovi[p2]);
        }
        else
        {
            qWarning() << "Upozorenje! Prepreka nije validna, preskace se";
        }
    }

    // Postavljanje pocetnog i krajnjeg cvora
    unsigned s, t;
    in >> s >> t;
    if (s < _cvorovi.size() && t < _cvorovi.size() && s != t)
    {
        _pocetak = &_cvorovi[s];
        _kraj = &_cvorovi[t];
    }
    else
    {
        qWarning() << "Upozorenje! Pocetak i kraj nisu validni cvorovi";
        _pocetak = &_cvorovi[0];
        _kraj = &_cvorovi[1];
    }
}

// Provera da li duz pocetak-kraj sece neku od prepreka iz prosledjene liste
bool Graf::secePrepreku(const QPointF* pocetak, const QPointF* kraj) const
{
    for (const auto& prepreka : _prepreke)
    {
        if(prepreka.sece(pocetak, kraj)) {
            return true;
        }
    }
    return false;
}

Routing::Routing(QWidget *pCrtanje,
                 int pauzaKoraka,
                 const bool &naivni,
                 std::string imeDatoteke,
                 int brojTacaka)
    : AlgoritamBaza(pCrtanje, pauzaKoraka, naivni),
      _graf(imeDatoteke, brojTacaka, pCrtanje ? pCrtanje->width() : CANVAS_WIDTH, pCrtanje ? pCrtanje->height() : CANVAS_HEIGHT)
{

}

void Routing::pokreniAlgoritam()
{
    // Test kod, crtanje grana iz svakog cvora posebno
//    for (auto &cvor : _tacke)
//    {
//        _put.push_back(&cvor);
//        AlgoritamBaza_updateCanvasAndBlock();
//    }

    // Blokiranje jedan frejm da se vidi samo graf za rutiranje
    AlgoritamBaza_updateCanvasAndBlock();

    // Petlja moze da se izvrsi ispocetka iako nije dodat cvor
    // ova promenljiva regulise da se blokiranje za iscrtavanje radi samo kada se doda cvor
    bool dodatCvor = true;

    _put.push_back(_graf.pocetak());

    while (_put.size() <= _graf.cvorovi().size()) // Provera beskonacne petlje, svaki cvor moze da se dostigne samo jednom
    {
        const Cvor *poslednji = _put.back();

        // Odredjivanje okoline za trenutni cvor
        if (_okoline.find(poslednji) == _okoline.end())
            _okoline.insert({poslednji, Okolina(_graf, poslednji, TipOkoline::CONSTRAINED_THETA_6)});

        if (dodatCvor)
            AlgoritamBaza_updateCanvasAndBlock();

        dodatCvor = false;

        // Ako smo stigli do cilja, izlazimo iz petlje
        if(poslednji == _graf.kraj())
            break;

        const Cvor *sledeci = nullptr;

        const auto &it = _okoline.find(poslednji);
        if (it != _okoline.end())
        {

            // Da li je trenutno u fazi izbegavanja
            if(!_pocetakFazeIzbegavanja)
            {
                // Nije u fazi izbegavanja prepreke

                sledeci = it->second.sledeciValidan(_graf.kraj());
                if (sledeci)
                {
                    // Nema prepreke prema cilju, krece se prema nablizem cvoru u tom regionu
                    // NO-OP
                }
                else
                {
                    // Ne postoje grane u regionu sa ciljem, ili su nevalidne
                    // Ulazi se u fazu izbegavanja prepreke
                    _pocetakFazeIzbegavanja = poslednji;
                    continue;
                }
            }
            else
            {
                // Vec je u fazi izbegavanja prepreke

                // Da li je poslednji cvor izvor prepreke od _pocetakFazeIzbegavanja do _kraj
                // ako jeste, proveravamo koji kraj prepreke je blizi _kraj
                // i nastavljamo rutiranje fazom teta-rutiranja
                bool zavrsioIzbegavanje = false;
                for (const auto &prepreka : it->second.prepreke())
                {
                    if (prepreka->sece(_pocetakFazeIzbegavanja, _graf.kraj()))
                    {
                        // Izlazak iz faze izbegavanja
                        zavrsioIzbegavanje = true;
                        _pocetakFazeIzbegavanja = nullptr;

                        // Prelazak na blizi kraj prepreke
                        QLineF izTrenutnog = QLineF(*poslednji, *_graf.kraj());
                        QLineF izDrugogKraja = QLineF(*prepreka->drugaStrana(poslednji), *_graf.kraj());
                        if(izDrugogKraja.length() < izTrenutnog.length())
                        {
                            sledeci = prepreka->drugaStrana(poslednji);
                        }

                        break;
                    }
                }
                if (zavrsioIzbegavanje && !sledeci)
                    continue;

                if (!zavrsioIzbegavanje)
                {
                    auto regionKraja = region(poslednji, _graf.kraj());
//                    auto regionKraja = region(_pocetakFazeIzbegavanja, _graf.kraj());

                    // Izbegavanje mora da je moguce iz dva pokusaja
                    // ako ne ka regionu cilja, onda prema sledecem regionu desno
                    for (unsigned i = 0; i < 2; i++)
                    {
                        // Neka je ciljni cvor u regionu 0
                        // biramo jednu od dva kandidata za sledecu granu:
                        // 1. grana prema najblizem cvoru u podregionu regiona 2 koji se granici sa regionom 1
                        // 2. grana u regionu 1 koja minimizuje ugao sa desnom granicom regiona 0
                        auto [kandidat1, kandidat2] = it->second.kandidatiZaIzbegavanjePrepreke(regionKraja.first+i);

                        if (kandidat1 && kandidat2)
                        {
                            // Ako je kandidat1 u (regionCilja+4)%M od kandidat2 i ne postoji prepreka iz poslednji
                            // koja sece unutrasnjost trougla poslednji,kandidat1,kandidat2 onda je sledeci kandidat1
                            // u suprotnom je kandidat2
                            bool premaKandidatu1 = true;
                            if (region(kandidat2, kandidat1).first == (regionKraja.first+i+4) % M)
                            {
                                for (const auto &prepreka : it->second.prepreke())
                                {
                                    const auto &drugiKraj = prepreka->drugaStrana(poslednji);
                                    if (pomocneFunkcije::orijentacija(*poslednji, *drugiKraj, *kandidat1) != pomocneFunkcije::orijentacija(*poslednji, *drugiKraj, *kandidat2))
                                    {
                                        premaKandidatu1 = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                premaKandidatu1 = false;
                            }

                            if (premaKandidatu1)
                            {
                                sledeci = kandidat1;
                            }
                            else
                            {
                                sledeci = kandidat2;
                            }
                        }
                        else if (kandidat1 && !kandidat2)
                        {
                            sledeci = kandidat1;
                        }
                        else if (kandidat2 && !kandidat1)
                        {
                            sledeci = kandidat2;
                        }
//                        else
//                        {
//                            // ne postoji nijedan kandidat, izbegavanje se nastavlja u regionu (regionCilja+1)%M
//                        }

                        if (sledeci)
                            break;
                    }
                }
            }
        }
        else
        {
            qCritical() << "Greska! Ne postoji okolina za cvor";
            break;
        }

        if (!sledeci)
        {
            qCritical() << "Greska! Put ne postoji";
            stampajGraf();
            break;
        }

        _put.push_back(sledeci);
        dodatCvor = true;
    }
    if(_put.size() > _graf.cvorovi().size())
    {
        qCritical() << "Greska! Beskonacna petlja";
        stampajGraf();
    }
}

void Routing::crtajAlgoritam(QPainter *painter) const
{
    if (!painter)
        return;

    QPen p = painter->pen();
    p.setColor(Qt::gray);
    p.setWidth(2);

    // Crtanje theta linija za regione
    if(!_put.empty())
    {
        const Cvor *trenutni = _put.back();

        // Crtanje granica regiona
        p.setStyle(Qt::PenStyle::SolidLine);
        painter->setPen(p);

        for(unsigned c = 0; c < M; c++)
        {
            double ugaoLinije = (90.0 + (THETA_ANGLE / 2) - c * THETA_ANGLE) / 180.0f * M_PI;
            QPointF tackaLinije = QPointF(trenutni->x() + 2*CANVAS_WIDTH*cos(ugaoLinije), trenutni->y() + 2*CANVAS_WIDTH*sin(ugaoLinije));
            painter->drawLine(*trenutni, tackaLinije);
        }

        // Crtanje sredine regiona
//        p.setStyle(Qt::PenStyle::DashLine);
//        painter->setPen(p);

//        for(unsigned c = 0; c < M; c++)
//        {
//            double ugaoLinije = (90.0 - (c * THETA_ANGLE)) / 180.0 * M_PI;
//            QPointF tackaLinije = QPointF(trenutni->x() + 2*CANVAS_WIDTH*cos(ugaoLinije), trenutni->y() + 2*CANVAS_WIDTH*sin(ugaoLinije));
//            painter->drawLine(*trenutni, tackaLinije);
//        }
    }

    p.setColor(Qt::blue);
    p.setWidth(2);
    p.setStyle(Qt::PenStyle::SolidLine);
    painter->setPen(p);

    // Crtanje celog grafa
//    for (const auto& okolina : _okoline)
//    {
//        okolina.second.crtajNajblize(painter);
//    }

    // Crtanje grana trenutnog cvora
    if(!_put.empty())
    {
        const Cvor *trenutni = _put.back();

        const auto &it = _okoline.find(trenutni);
        if (it != _okoline.end())
        {
            it->second.crtajSusede(painter);
        }
    }

    // Crtanje puta
    p.setColor(Qt::magenta);
    p.setWidth(3);
    p.setStyle(Qt::PenStyle::SolidLine);
    painter->setPen(p);

    for (unsigned i = 1; i < _put.size(); i++)
    {
        painter->drawLine(*_put[i-1], *_put[i]);
    }

    // Crtanje prepreka
    p.setColor(Qt::darkGray);
    p.setWidth(3);
    p.setStyle(Qt::PenStyle::SolidLine);
    painter->setPen(p);

    for (const auto& prepreka : _graf.prepreke())
    {
        prepreka.crtaj(painter);
    }

    // Crtanje cvorova
    p.setColor(Qt::black);
    p.setWidth(10);
    painter->setPen(p);
    for(const auto& tacka : _graf.cvorovi())
    {
        painter->drawPoint(tacka);
    }

    // Pocetni cvor
    p.setColor(Qt::green);
    painter->setPen(p);
    painter->drawPoint(*_graf.pocetak());

    // Krajnji cvor
    p.setColor(Qt::red);
    painter->setPen(p);
    painter->drawPoint(*_graf.kraj());
}

void Routing::pokreniNaivniAlgoritam()
{
    // Odredjivanje okoline za svaki cvor
    for (const auto &cvor : _graf.cvorovi())
    {
        for (const auto &susedniCvor : _graf.cvorovi())
        {
            // preskacemo trenutni cvor
            if (&cvor == &susedniCvor)
                continue;

            if (!_graf.secePrepreku(&cvor, &susedniCvor))
            {
                _grane[&cvor].push_back(&susedniCvor);
            }
        }
    }

    AlgoritamBaza_updateCanvasAndBlock();

    _stek.push_back(_graf.pocetak());
    _roditelj[_graf.pocetak()] = nullptr;
    _posecen[_graf.pocetak()] = false;

    while (!_stek.empty())
    {        
        AlgoritamBaza_updateCanvasAndBlock();

        const Cvor *poslednji = _stek.back();
        _stek.pop_back();

        if (poslednji == _graf.kraj())
            break;

        if (!_posecen[poslednji])
        {
            _posecen[poslednji] = true;

            for (const auto &sused : _grane[poslednji])
            {
                if (!_posecen[sused])
                {
                    _roditelj[sused] = poslednji;
                    _stek.push_back(sused);
                }
            }
        }
    }

    if(_stek.empty())
        qCritical() << "Greska! Put nije pronadjen";

    AlgoritamBaza_updateCanvasAndBlock();
}

void Routing::crtajNaivniAlgoritam(QPainter *painter) const
{
    if (!painter)
        return;

    QPen p = painter->pen();

    p.setColor(Qt::blue);
    p.setWidth(2);
    p.setStyle(Qt::PenStyle::SolidLine);
    painter->setPen(p);

    // Crtanje celog grafa
    for (const auto& par : _grane)
    {
        for (const auto& sused : par.second)
        {
            painter->drawLine(*par.first, *sused);
        }
    }

    // Crtanje puta
    if (!_stek.empty())
    {
        p.setColor(Qt::magenta);
        p.setWidth(3);
        p.setStyle(Qt::PenStyle::SolidLine);
        painter->setPen(p);

        const Cvor *prethodni = nullptr;
        const Cvor *trenutni = _stek.back();
        while ((prethodni = _roditelj.find(trenutni)->second) != nullptr)
        {
            painter->drawLine(*prethodni, *trenutni);
            trenutni = prethodni;
        }
    }

    // Crtanje prepreka
    p.setColor(Qt::darkGray);
    p.setWidth(3);
    p.setStyle(Qt::PenStyle::SolidLine);
    painter->setPen(p);

    for (const auto& prepreka : _graf.prepreke())
    {
        prepreka.crtaj(painter);
    }

    // Crtanje cvorova
    p.setColor(Qt::black);
    p.setWidth(10);
    painter->setPen(p);
    for(const auto& tacka : _graf.cvorovi())
    {
        painter->drawPoint(tacka);
    }

    // Pocetni cvor
    p.setColor(Qt::green);
    painter->setPen(p);
    painter->drawPoint(*_graf.pocetak());

    // Krajnji cvor
    p.setColor(Qt::red);
    painter->setPen(p);
    painter->drawPoint(*_graf.kraj());
}

std::vector<unsigned> Routing::pronadjeniPut() const
{
    std::vector<unsigned> put;
    put.reserve(_put.size());

    std::unordered_map<const Cvor*, unsigned> indexLookup;
    for (unsigned i = 0; i < _graf.cvorovi().size(); i++)
    {
        indexLookup[_graf.cvor(i)] = i;
    }

    for (const auto cvor : _put)
    {
        put.push_back(indexLookup[cvor]);
    }

    return put;
}

void Routing::stampajGraf() const
{
    qCritical() << _graf.cvorovi().size() << " " << _graf.prepreke().size();

    std::unordered_map<const Cvor*, unsigned> indexLookup;
    for (unsigned i = 0; i < _graf.cvorovi().size(); i++)
    {
        qCritical() << _graf.cvor(i)->x() << " " << _graf.cvor(i)->y();
        indexLookup[_graf.cvor(i)] = i;
    }

    for (const auto &prepreka : _graf.prepreke())
    {
        qCritical() << indexLookup[prepreka.p1()] << " " << indexLookup[prepreka.p2()];
    }

    qCritical() << indexLookup[_graf.pocetak()] << " " << indexLookup[_graf.kraj()];
}
