QT       += core gui opengl charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
LIBS += -lglut -lGLU
CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/algoritambaza.cpp \
    src/algoritmi_sa_vezbi/ga00_demoiscrtavanja.cpp \
    src/algoritmi_sa_vezbi/ga01_brisucaprava.cpp \
    src/algoritmi_sa_vezbi/ga02_3discrtavanje.cpp \
    src/algoritmi_sa_vezbi/ga03_konveksniomotac.cpp \
    src/algoritmi_sa_vezbi/ga04_konveksniomotac3d.cpp \
    src/algoritmi_sa_vezbi/ga05_preseciduzi.cpp \
    src/algoritmi_sa_vezbi/ga06_dcel.cpp \
    src/algoritmi_sa_vezbi/ga06_dceldemo.cpp \
    src/algoritmi_sa_vezbi/ga07_triangulation.cpp \
    src/algoritmi_studentski_projekti/ga06_presekPravougaonika.cpp \
    src/algoritmi_studentski_projekti/ga08_routing.cpp \
    src/animacijanit.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/oblastcrtanja.cpp \
    src/oblastcrtanjaopengl.cpp \
    src/pomocnefunkcije.cpp \
    src/timemeasurementthread.cpp

HEADERS += \
    src/algoritambaza.h \
    src/algoritmi_sa_vezbi/ga00_demoiscrtavanja.h \
    src/algoritmi_sa_vezbi/ga01_brisucaprava.h \
    src/algoritmi_sa_vezbi/ga02_3discrtavanje.h \
    src/algoritmi_sa_vezbi/ga03_konveksniomotac.h \
    src/algoritmi_sa_vezbi/ga04_konveksni3dDatastructures.h \
    src/algoritmi_sa_vezbi/ga04_konveksniomotac3d.h \
    src/algoritmi_sa_vezbi/ga05_datastructures.h \
    src/algoritmi_sa_vezbi/ga05_preseciduzi.h \
    src/algoritmi_sa_vezbi/ga06_dcel.h \
    src/algoritmi_sa_vezbi/ga06_dceldemo.h \
    src/algoritmi_sa_vezbi/ga07_datastructures.h \
    src/algoritmi_sa_vezbi/ga07_triangulation.h \
    src/algoritmi_studentski_projekti/ga06_presekPravougaonika.h \
    src/algoritmi_studentski_projekti/ga08_routing.h \
    src/animacijanit.h \
    src/config.h \
    src/mainwindow.h \
    src/oblastcrtanja.h \
    src/oblastcrtanjaopengl.h \
    src/pomocnefunkcije.h \
    src/timemeasurementthread.h

INCLUDEPATH += src

FORMS += \
    src/mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    docs/ga06_presekPravougaonika.docx \
    docs/ga06_presekPravougaonika.pdf \
    input_files/ga00_DCELDemo/mushroom.off \
    input_files/ga00_DCELDemo/test0.off \
    input_files/ga00_KonveksniOmotac3D/input1.txt \
    input_files/ga06_presekPravougaonika/input1.txt \
    input_files/ga06_presekPravougaonika/input2.txt \
    input_files/ga06_presekPravougaonika/input3.txt
