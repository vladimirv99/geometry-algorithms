\documentclass[a4paper]{article}

\usepackage[english,serbian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{color}
\usepackage{url}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\graphicspath{ {images/} }
\usepackage{tikz}
\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\begin{document}

\title{Algoritam za rutiranje na grafu vidljivosti\\ \small{Seminarski rad u okviru kursa\\Geometrijski algoritmi\\Matematički fakultet}}

\author{Vladimir Vuksanović, 1006/2021}

\maketitle

\abstract{
Ovo je prateća dokumentacija za algoritam rutiranja u okviru projekta \href{https://bitbucket.org/vladimirv99/geometry-algorithms}{https://bitbucket.org/vladimirv99/geometry-algorithms}. U nastavku je opisan problem koji se rešava, detalji implementacije naivnog i efikasnog algoritama i njihove vizuelizacije. Uz efikasni algoritam su priloženi i testovi za njegovu korektnost.
}

\tableofcontents

\newpage

\section{Opis problema}

Rutiranje je postupak slanja poruke kroz mrežu od izvornog do ciljnog čvora.
Neka imamo graf u ravni čiji je skup čvorova $P$ i skup grana $S$.
Označimo jedan čvor iz $P$ kao početak i jedan kao kraj.
Grane iz $S$ predstavljaju prepreke koje ometaju slanje poruke.
Ovaj graf je ulaz za oba algoritma.
Jedan njegov primer je prikazan na slici \ref{fig:routing_graph}.

\begin{figure}[h!]
    \begin{center}
        \includegraphics[scale=0.25]{routing_graph.png}
    \end{center}
    \caption{Primer grafa za rutiranje. Zelenom bojom je označen početni, a crvenom ciljni čvor}
    \label{fig:routing_graph}
\end{figure}

Ako za dva čvora $u$ i $v$ postoji duž koja ih spaja, a ne preseca nijednu prepreku (ne uzimajući u obzir krajeve prepreka) ili ako postoji prepreka čiji su krajevi $u$ i $v$ onda kažemo da se čvorovi $u$ i $v$ međusobno vide.
U tom slučaju se grana $uv$ naziva \textbf{grana vidljivosti}.
Na osnovu grafa $G = (P, S)$ možemo da konstruišemo \textbf{graf vidljivosti} $Vis(P, S)$ čiji skup čvorova je $P$ a skup grana su sve grane vidljivosti početnog grafa $G$.
Cilj je pronaći put od početnog do krajnjeg čvora na grafu $Vis(P, S)$.
Primer grafa vidljivosti je prikazan na slici \ref{fig:visibility_graph}.

\begin{figure}[h!]
    \begin{center}
        \includegraphics[scale=0.25]{visibility_graph.png}
    \end{center}
    \caption{Primer grafa vidljivosti za graf iz slike \ref{fig:routing_graph}. Sive linije su prepreke, a plave su grane grafa vidljivosti}
    \label{fig:visibility_graph}
\end{figure}

Uvedeni su alijasi \verb|Cvor| i \verb|Linija| za redom \verb|QPointF| i \verb|QLineF|.
Za implementaciju prepreka je uvedena klasa \verb|Prepreka| koja omogućuje jednostavnu proveru da li ona seče put između dva čvora i lako dohvatanje suprotnog kraja te prepreke.
Ceo ulaz je enkapsuliran u strukturu \verb|Graf| koja osim potrebnih polja sadrži i pomoćnu funkciju \verb|secePrepreku| za proveru da li duž određena prosleđenim čvorovima seče neku od prepreka.

\section{Naivni algoritam}

Naivni algoritam podrazumeva globalno znanje o celom grafu i dovoljno memorije u poruci da sadrži podatke o svakom čvoru na putu.
Prvo se inicijalizuju susedi svakog čvora tako što se za svakog njegovog mogućeg suseda proverava da li seče neku od prepreka.
Ako seče onda taj čvor nije sused trenutnom, u suprotnom jeste i dodaje se grana ka njemu.
Kada je graf vidljivosti konstruisan onda se traženje ciljnog čvora svodi na algoritme poput DFS, BFS ili Dajkstrinog algoritma.
U kodu je izabran algoritam pretrage u dubinu.

\begin{figure}[h!]
    \begin{center}
        \includegraphics[scale=0.25]{naive_algorithm.png}
    \end{center}
    \caption{Put koji pronalazi naivni algoritam (označen ljubičastom bojom)}
    \label{fig:naive_algorithm}
\end{figure}

\section{Efikasni algoritam}

Efikasni algoritam je implementiran na osnovu opisa u \cite{bose2018routing}.
On radi koristeći samo lokalne informacije u svakom čvoru uz konstantnu količinu memorije unutar poruke.

Umesto direktno na grafu vidljivosti, ovaj algoritam radi rutiranje na podgrafu koji se naziva \textbf{ograničeni $\Theta_6$-graf}.
Taj podgraf u svakom čvoru deli ravan na 6 jednakih regiona, označenih $C_0$ do $C_5$, kao na slici \ref{fig:theta6_cones}.
Svaki region se dalje deli na podregione ukoliko iz trenutnog čvora postoji prepreka čiji je drugi kraj u tom regionu.
Za svaki od podregiona se dodaje po jedna grana do najbližeg čvora koji mu pripada. Udaljenost se meri duž poluprave koja polovi region.
Ukoliko je čvor drugi kraj prepreke koja deli region na podregione onda se smatra da on pripada oba podregiona.
Ove veze su implementirane pomoću struktura: \verb|Okolina|, \verb|Region| i \verb|Podregion|.
Okolina inicijalizuje 6 regiona, a svaki region inicijalizuje podregione u zavisnosti od toga koliko ima prepreka u tom regionu.
Na raspolaganju su i pomoćne funkcije \verb|region| koja računa region i ugao čvora (počevši od leve strane prvog regiona) u odnosu na drugi i \verb|udaljenost| koja računa udaljenost čvora u odnosu na neki drugi na prethodno opisani način.

\begin{figure}[h!]
    \begin{center}
        \input{images/theta6_cones.tikz}
    \end{center}
    \caption{Regioni $\Theta_6$ grafa}
    \label{fig:theta6_cones}
\end{figure}

Pre rutiranja potrebno je konstruisati ovaj graf. Zadatak je u svakom čvoru lokalno odrediti da li mu je neki drugi čvor najbliži i da li je ovaj čvor najbliži nekom drugom.
Prvi slučaj je jednostavan za implenetaciju: u svakom podregionu se konstantno ažurura najbliži čvor koji mu pripada i kada se prođu svi mogući susedi, najbliži se doda u listu suseda.
Drugi slučaj zahteva proveru više uslova. Neka čvor $u$ treba da proveri da li je on najbliži čvoru $v$ u nekom regionu.
To važi ako ne postoji čvor $x$ u ograničenom kanonskom trouglu $\triangle_{vu}$ takav da $x$ vidi $u$.
\textbf{Kanonski trougao} $\triangle_{vu}$ je trougao ograničen granicama regiona čvora $v$ u kojem se nalazi $u$ i normalom na polupravu koja polovi taj region i prolazi kroz $u$.
\textbf{Ograničeni kanonski trougao} je deo kanonskog trougla koji je sasečen preprekama iz čvora $u$ čiji drugi kraj ne pripada trouglu.
Pripadnost kanonskom trouglu je implementirana poređenjem jednakosti regiona čvorova i merenjem udaljenosti od $v$.
Provera da li se čvor nalazi u odsečenom delu se radi poređenjem orijentacije trouglova čija su dva čvora krajevi prepreke, a treći čvorovi su $x$ i $v$.

Algoritam rutiranja se sastoji iz 3 faze:
\begin{enumerate}
    \item \textbf{$\Theta$-rutiranje} -- sve dok je grana prema najbližem čvoru u podrgionu validna, kreće se prema njoj. Za granu se kaže da je nevalidna ako je čvor kojem vodi dalji od ciljnog u odnosu na trenutni čvor. Ukoliko grana ne postoji ili je nevalidna, onda sigurno postoji prepreka između trenutnog čvora i cilja i prelazi se u fazu izbegavanja prepreke
    \item \textbf{Izbegavanje prepreke} -- kada postoji prepreka između trenutnog čvora i cilja, počinje se rutiranje ka desnom kraju prepreke.
    Bez smanjenja opštosti, neka je cilj u regionu $C_0$ čvora $u$ koji je izazavo prelazak u ovu fazu i neka je $m$ trenutni čvor na putu do kraja prepreke. 
    Za sledeći čvor se bira jedan od sledećih kandidata:
    \begin{itemize}
        \item najbliži vidljivi čvor $v$ iz $C_2^m$ koji ima zajedničku granicu sa $C_1^m$
        \item čvor $w$ iz $C_1^m$ koji minimizuje ugao između $mw$ i desne granice $C_0^m$
    \end{itemize}
    Ako neki kandidat ne postoji, bira se onaj drugi.
    Ako oba postoje i $v$ se nalazi u $C_4^w$ i ne postoji prepreka iz $m$ koja seče unutrašnjost trougla $mvw$ onda se bira $v$, a u suprotnom $w$.
    Ako nijedan kandidat ne postoji onda se počinje nova faza izbegavanja prema $C_1$ umesto $C_0$.
    Da bi se detektovalo dostizanje kraja prepreke, čvor $u$ se čuva poruci koja se prosleđuje i kada se dođe u neki čvor proveravaju se sve incidentne prepreke da li seku duž $ut$.
    Kada je kraj dostignut, prelazi se u fazu traženja drugog kraja prepreke.
    \item \textbf{Traženje drugog kraja prepreke} -- ako se prilikom dostizanja kraja prepreke primeti da je drugi kraj bliži cilju, onda se drugi kraj uzima za sledeći na putu. To je moguće zato što su dva kraja prepreke sigurno povezani u grafu vidljivosti. Kada se pronađe bliži kraj, vraća se na fazu $\Theta$-rutiranja
\end{enumerate}

Dohvatanje najbližeg čvora, kao i kandidata za izbegavanje prepreke rade funkcije \verb|sledeciValidan| i \verb|kandidatiZaIzbegavanjePrepreke| iz klase \verb|Okolina|, a sve ostalo se radi u funkciji \verb|pokreniAlgoritam|.
Uz svaki čvor u okolini se čuva njegov ugao u odnosu na trenutni, i svaki podregion ima pokazivač ka najbližem čvoru.
Ovo omogućava lako dohvatanje potrebnih suseda, a ostatak implementacije se svodi na direktno praćenje prethodnih koraka.

Rad algoritma je prikazan na slikama \ref{fig:optimal_algorithm_start} i \ref{fig:optimal_algorithm_stop}.

\begin{figure}[h!]
    \begin{center}
        \includegraphics[scale=0.25]{optimal_algorithm_start.png}
    \end{center}
    \caption{Početak izvršavanja efikasnog algoritma}
    \label{fig:optimal_algorithm_start}
\end{figure}

\begin{figure}[h!]
    \begin{center}
        \includegraphics[scale=0.25]{optimal_algorithm_stop.png}
    \end{center}
    \caption{Kraj izvršavanja efikasnog algoritma}
    \label{fig:optimal_algorithm_stop}
\end{figure}

\section{Poređenje efiksanosti algoritama}

Poređenje efikasnosti ovih algoritama nema puno smisla. Za efikasni algoritam važe ograničenja koja nisu prisutna u naivnom algoritmu (lokalnost i konstantna količina memorije).
Implementacija naivnog algoritma je jednostavnija što je čini bržom, ali efikasni algoritam potencijalno (ne i garantovano) razmatra manje čvorova zato što koristi strukturu grafa da usmeri rutiranje.
Grafik brzine izvršavanja programa u zavisnosti od veličine ulaza je prikazan na slici \ref{fig:efficiency_comparison}.
Najveća prednost efikasnog algoritma je što se okolina računa samo za čvorove na putu, a ne za ceo graf, jer taj deo uzima najviše vremena.

\begin{figure}[h!]
    \begin{center}
        \includegraphics[scale=0.25]{efficiency_comparison.png}
    \end{center}
    \caption{Poređenje efikasnosti naivnog i optimalnog algoritma}
    \label{fig:efficiency_comparison}
\end{figure}

\section{Testiranje}

Za testiranje algoritma koristi se \href{https://github.com/google/googletest}{googletest} biblioteka.
Projekat podrazumeva da se njen repozitorijum nalazi u direktorijumu iznad onog sa \verb|.pro| fajlovima.
Testovi su deo projekta \verb|GA_test.pro| i nalaze se u fajlu \verb|test/tst_routing.h|.
Ulazni fajlovi za testove se nalaze u \verb|input_files| direktorijumu.

Testirani su sledeći delovi algoritma:
\begin{itemize}
    \item rutiranje na grafu bez prepreka (ispravnost $\Theta$-rutiranja)
    \item rutiranje sa preprekom (ispravnost ulaska u fazu izbegavanje prepreke)
    \item izbor bližeg kraja prepreke prema cilju
    \item odabir kandidata za izbegavanje prepreke za svaku kombinaciju postojanja oba kandidata
\end{itemize}

\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{ga08_rutiranjeNaGrafuVidljivosti}
\bibliographystyle{ieeetr}

\end{document}

