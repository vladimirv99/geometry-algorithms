include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG += thread
CONFIG += qt

QT += core gui charts widgets opengl

HEADERS += \
    src/algoritambaza.h \
    src/animacijanit.h \
    src/config.h \
    src/pomocnefunkcije.h \
    src/algoritmi_studentski_projekti/ga08_routing.h

SOURCES += \
    src/algoritambaza.cpp \
    src/animacijanit.cpp \
    src/pomocnefunkcije.cpp \
    src/algoritmi_studentski_projekti/ga08_routing.cpp \
    test/main.cpp \
    test/tst_routing.cpp

INCLUDEPATH += \
    src/ \
    src/algoritmi_studentski_projekti
