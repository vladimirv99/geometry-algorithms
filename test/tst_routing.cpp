
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "ga08_routing.h"

using namespace testing;

TEST(Routing, no_obstacles)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input1.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 3, 7};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, obstacle_avoidance_phase)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input2.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 2, 3};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, closest_obstacle_end_phase)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input3.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 2, 1, 3};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, obstacle_avoidance_candidate_1_only)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input4.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 3, 1, 4};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, obstacle_avoidance_candidate_2_only)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input5.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 3, 1, 4};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, obstacle_avoidance_both_candidates_1)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input6.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 4, 1, 5};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, obstacle_avoidance_both_candidates_2)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input7.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 3, 1, 5};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

TEST(Routing, obstacle_avoidance_no_candidates)
{
    Routing routing { nullptr, 0, false, "../input_files/ga08_routing/input8.txt" };
    routing.pokreniAlgoritam();
    auto pronadjeniPut = routing.pronadjeniPut();

    std::vector<unsigned> tacanPut {0, 1, 3};

    EXPECT_THAT(pronadjeniPut, ContainerEq(tacanPut));
}

//TEST(Routing, random_graphs)
//{
//    for (unsigned i = 0; i < 10000; i++)
//    {
//        Routing routing { nullptr, 0, false, "", 20 };
//        routing.pokreniAlgoritam();
//        auto pronadjeniPut = routing.pronadjeniPut();

//        EXPECT_EQ(pronadjeniPut.back(), 1);
//    }
//}
